'use strict'

const slidesArray = ['airmax-jump.png', 'airmax-on-foot.png', 'airmax-playground.png', 'airmax-top-view.png', 'airmax.png'];
let index = 0;
let slider = document.getElementById('slider');
slider.setAttribute('src', `i/${slidesArray[index]}`); 

setInterval (function(){
index++;
if (index >= slidesArray.length){
  index = 0;
}
slider.setAttribute('src', `i/${slidesArray[index]}`); 
}, 5000);