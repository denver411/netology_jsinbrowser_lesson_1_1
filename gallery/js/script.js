'use strict'

const images = ['breuer-building.jpg', 'guggenheim-museum.jpg', 'headquarters.jpg', 'IAC.jpg', 'new-museum.jpg'];
let slider = document.getElementById('currentPhoto');
let buttonNext = document.getElementById('nextPhoto');
let buttonPrev = document.getElementById('prevPhoto');
let currentPhotoIndex = 0;
slider.setAttribute('src', `i/${images[currentPhotoIndex]}`);

buttonNext.onclick = function () {
  currentPhotoIndex++;
  if (currentPhotoIndex >= images.length) {
    currentPhotoIndex = 0;
  }
  slider.src = `i/${images[currentPhotoIndex]}`;
}

buttonPrev.onclick = function () {
  currentPhotoIndex--;
  if (currentPhotoIndex < 0) {
    currentPhotoIndex = images.length - 1;
  }
  slider.src = `i/${images[currentPhotoIndex]}`;
}